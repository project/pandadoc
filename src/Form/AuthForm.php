<?php

namespace Drupal\pandadoc\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AuthForm.
 */
class AuthForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'pandadoc.auth',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'auth_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pandadoc.auth');
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key :'),
      '#required' => TRUE,
      '#description' => $this->t('To get key follow <a href="https://developers.pandadoc.com/reference/api-key-authentication-process" target="_blank">API Key Authentication Process</a>'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('pandadoc.auth')
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();
  }

}
